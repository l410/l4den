.PHONY: docs

default: docs build

build:

docs: l4den.pdf

%.pdf: %.tex
	arara $^

clean: clean-l4den.pdf

clean-%.pdf:
	-rm -f $*.aux $*.idx $*.ilg $*.ind $*.log $*.out $*.toc
